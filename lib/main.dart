import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String battery = "?";
  static const methodChannel =
      MethodChannel('batteryMethodChannel');

  @override
  void initState() {
    _getBatteryLevel();
    super.initState();
  }

  Future<void> _getBatteryLevel() async {
    try {
      final int batterylevel = await methodChannel.invokeMethod('battery2', {'a':'b','key':'value'});
      battery = batterylevel.toString();
    } on PlatformException catch (e) {
      print(e.message);
      battery = 'Error de bateria';
    } on MissingPluginException catch (e) {
      battery = 'Error de plugin';
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bateria"),
      ),
      body: Center(
        child: Text("Bateria $battery"),
      ),
    );
  }
}
