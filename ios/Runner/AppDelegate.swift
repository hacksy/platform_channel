import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {



    let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
    let batteryChannel = FlutterMethodChannel(name: "batteryMethodChannel", binaryMessenger: controller.binaryMessenger)

    batteryChannel.setMethodCallHandler({
     (call: FlutterMethodCall, result: @escaping FlutteResult) -> Void in
        guard call.method == 'battery' else{
            result(FlutterMethodNotImplemented)
            return
        }
        self?.getBatteryLevel(result:result)

    })


    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)

  }

  private func getBatteryLevel(result: FlutterResult){
    let device = UIDevice.current
    device.isBatteryMonitoringEnabled =true
    if device.batteryState == UIDevice.BatteryState.unknown {
        result(FlutterError(code:"X",message:"BatteryError2",details:nil))
    }else{
    result(Int(device.batteryLevel * 100))
  }
}
