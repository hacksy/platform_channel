package dev.hacksy.platform_example

import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    private val CHANNEL = "batteryMethodChannel";

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler{
            call, result->
            if(call.method == "battery") {
                android.util.Log.w("BATTERTAG", call.arguments.toString());
                val battery = getBatteryLevel();
                android.util.Log.w("BATTERTAG", battery.toString());

                if(battery != -1){
                    result.success(battery);
                }else{
                    result.error("NODISPONIBLE","Error de bateria2",null);
                }

            }else  if(call.method == "battery2"){
                result.success("Bateria falsa!!");
           }else{
                result.notImplemented();
            }
        };
    }

    private fun getBatteryLevel() : Int{
        val batteryLevel: Int
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val batteryManager = getSystemService(BATTERY_SERVICE) as BatteryManager;
            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        }else{
            val intent = ContextWrapper(applicationContext).registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            batteryLevel = intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL,-1)*100/intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        }
        return batteryLevel
    }
}
